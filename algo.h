struct Point;

/** Performs one step of k-means clusterization for points within given range [begin..end),
* and returns cluster IDs for them as well as new cluster centers (a kind of sums) and counts of points in them.
* @param K is the number of clusters
*/
void kmeans_step(size_t begin, size_t end, const Point points[], size_t cluster_ids[]
    , size_t K, const Point centroids[], Point centros[], size_t counts[]);

/** Performs k-means clusterization for given points, grouping them into K separate clusters.
* @param N is the number of points
* @param cluster_ids will get cluster ID for each point
* @param K is the number of clusters
* @param centroid will get centers of each cluster
* @return Number of iterations
*/
size_t kmeans(size_t N, const Point points[/*N*/], size_t cluster_ids[/*N*/], size_t K, Point centroids[/*K*/], size_t num_threads = 1);

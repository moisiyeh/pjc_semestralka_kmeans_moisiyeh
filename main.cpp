
#include <cmath>
#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <string>
#include <thread>

#include "algo.h"
#include "Dataset.h"

namespace timer {

    auto now() {
        return std::chrono::high_resolution_clock::now();
    }

    template<typename TimePoint>
    long long to_ms(TimePoint tp) {
        return std::chrono::duration_cast<std::chrono::milliseconds>(tp).count();
    }

}


void print_usage(std::string const &exe_name) {
    std::clog << "Usage: " << exe_name
              << " --clusters K --input FILENAME --output FILENAME [--centroids FILENAME] [--threads N]\n"
              << "Arguments : K - number of clusters;\n"
                 "input FILENAME - filename to read points from, required;\n"
                 "output FILENAME -  filename to write points with cluster IDs, required;\n"
                 "centroids FILENAME - filename to write centroids, optional;\n"
                 "threads N - number of threads, optional, default value - 1.\n"
                 "Usage : --help / -h";
}

bool is_help(std::string const &argument) {
    return argument == "--help" || argument == "-h";
}

int main(int argc, const char *argv[]) {
    // Values which can be specified in the command line
    unsigned num_clusters = 0;    // number of clusters, required
    unsigned num_threads = 0;    // number of threads to use, optional, default is 1
    std::string in_fname;           // filename to read points from, required
    std::string out_fname;          // filename to write points with cluster IDs, required
    std::string out_centro_fname;   // filename to write centroids, optional

    if (std::any_of(argv, argv + argc, is_help)) {
        print_usage(argv[0]);
        return 0;
    }

    // Iterate through all command line switches
    for (int i = 1; i < argc; ++i) {
        const std::string arg = argv[i];
        if (arg == "--clusters") {
            if (i + 1 < argc)
                num_clusters = strtoul(argv[i + 1], nullptr, 10);
            if (!num_clusters) {
                std::clog << "Number of clusters is expected.\n";
                print_usage(argv[0]);
                return 1;
            }
            ++i;
        } else if (arg == "--input") {
            if (i + 1 < argc)
                in_fname = argv[i + 1];
            if (in_fname.empty()) {
                std::clog << "File name should follow '" << arg << "'.\n";
                print_usage(argv[0]);
                return 1;
            }
            ++i;
        } else if (arg == "--output") {
            if (i + 1 < argc)
                out_fname = argv[i + 1];
            if (out_fname.empty()) {
                std::clog << "File name should follow '" << arg << "'.\n";
                print_usage(argv[0]);
                return 1;
            }
            ++i;
        } else if (arg == "--centroids") {
            if (i + 1 < argc)
                out_centro_fname = argv[i + 1];
            if (out_centro_fname.empty()) {
                std::clog << "File name should follow '" << arg << "'.\n";
                print_usage(argv[0]);
                return 1;
            }
            ++i;
        } else if (arg == "--threads") {
            if (i + 1 < argc)
                num_threads = strtoul(argv[i + 1], nullptr, 10);
            if (!num_threads) { // zero means the value is missing or invalid (not a number, etc.)
                std::clog << "Number of threads is expected.\n";
                print_usage(argv[0]);
                return 1;
            }
            ++i;
        } else {
            std::clog << "Unsupported switch " << arg << std::endl;
            print_usage(argv[0]);
            return 1;
        }
    }

    if (in_fname.empty()) {
        std::clog << "Input file name is required!\n";
        print_usage(argv[0]);
        return 1;
    }
    if (out_fname.empty()) {
        std::clog << "Output file name is required!\n";
        print_usage(argv[0]);
        return 1;
    }
    if (!num_clusters) {
        std::clog << "Number of clusters is required!\n";
        print_usage(argv[0]);
        return 1;
    }
    if (!num_threads)
        num_threads = 1;

    std::cout << "Number of clusters:   " << num_clusters << std::endl;
    std::cout << "Input file name:      " << in_fname << std::endl;
    std::cout << "Output file name:     " << out_fname << std::endl;
    if (!out_centro_fname.empty())
        std::cout << "Centroids file name:  " << out_centro_fname << std::endl;
    std::cout << "Number of threads:    " << num_threads << std::endl;

    std::ifstream in_stream(in_fname);
    if (!in_stream.good()) {
        std::clog << "Cannot open '" << in_fname << "' for reading!\n";
        return 1;
    }
    Dataset dataset;
    in_stream >> dataset;
    const auto num_points = dataset.size();
    if (num_points < num_clusters) {
        std::clog << "Not enough points in data set!\n";
        return 1;
    }
    std::cout << "Number of points:   " << num_points << std::endl;

    // Cluster ID for each point
    std::unique_ptr<size_t> cluster_ids(new size_t[num_points]);
    std::unique_ptr<Point> centroids(new Point[num_clusters]);

    const Point *const points = dataset.begin();

    const auto time_start = timer::now();

    const auto num_iterations = kmeans(num_points, points, cluster_ids.get(), num_clusters, centroids.get(),
                                       num_threads);

    const auto time_end = timer::now();

    // Let's check whether all points were assigned to clusters
    size_t missed = 0;
    for (size_t i = 0; i < num_points; ++i)
        if (cluster_ids.get()[i] >= num_clusters)   // if invalid cluster ID is assigned
            ++missed;
    if (missed)
        std::cout << "WARNING: number of points out of clusters = " << missed << " (" << 100.0 * missed / num_points
                  << "%)\n";

    if (!out_fname.empty()) {
        std::ofstream out_stream(out_fname);
        if (out_stream.good()) {
            for (size_t i = 0; i < num_points; ++i) {
                out_stream << points[i] << ' ' << cluster_ids.get()[i] << std::endl;
            }
        } else
            std::clog << "Cannot open file '" << out_fname << "' for writing points with cluster IDs!\n";
    }

    if (!out_centro_fname.empty()) {
        std::ofstream centro_stream(out_centro_fname);
        if (centro_stream.good()) {
            for (size_t k = 0; k < num_clusters; ++k)
                centro_stream << centroids.get()[k] << std::endl;
        } else
            std::clog << "Cannot open file '" << out_centro_fname << "' for writing centroids!\n";
    }

    std::cout << "Iterations: " << num_iterations << std::endl;
    std::cout << "Time spent: " << timer::to_ms(time_end - time_start) << " ms\n";

    return 0;
}

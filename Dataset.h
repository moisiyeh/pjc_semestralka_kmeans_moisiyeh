#pragma once

#include <memory>

#include "Point.h"

class Dataset {
    std::unique_ptr<Point[]> _points;
    size_t _size;
public:
    const Point* begin() const {
        return _points.get();
    }
    const Point* end() const {
        return _points.get() + _size;
    }
    size_t size() const {
        return _size;
    }

    void read(std::istream& is);
};
inline std::istream& operator >> (std::istream& is, Dataset& obj) {
    obj.read(is);
    return is;
}

# pjc_semestralka_kmeans_moisiyeh
1) For 9_points.txt - 2 clusters
--clusters 2 --input data/9_points.txt --output output/res.out --centroids output/centroids.out

2) For 5000_points.txt - 15 clusters
--clusters 15 --input data/5000_points.txt --output output/res.out --centroids output/centroids.out --threads 2


3) For 100000_points.txt - 100 clusters
--clusters 100 --input data/100000_points.txt --output output/res.out --centroids output/centroids.out --threads 8

CMake list uděla kopii složky se vstupními daty.
input/...

Output není nutná složka. 

import matplotlib.pyplot as plt
import numpy as np
import sys

data = np.loadtxt(sys.argv[1]) # Pass file name with clusterization result as 1-st parameter

x, y, Clusters = data[:, 0], data[:, 1], data[:, 2]

centers = np.loadtxt(sys.argv[2]) # Pass file name with centroids as 2-nd parameter

#x = np.random.randn(10)
#y = np.random.randn(10)
#Clusters = np.array([0, 1, 1, 1, 3, 2, 2, 3, 0, 2])    # Labels of cluster 0 to 3
#centers = np.random.randn(4, 2) 

fig = plt.figure()
ax = fig.add_subplot(111)
#colors = colors = { 0: 'red', 1: 'green', 2: 'blue', 3: 'yellow', 4: 'black', 5: 'violet', 6: 'cyan', 7: 'orange' }
# RdYlBu
scatter = ax.scatter(x, y, c = Clusters, cmap = 'gist_rainbow', s = 1)

for i, j in centers:
    ax.scatter(i, j, s = 5, c = 'black', marker = '+')

ax.set_xlabel('x')
ax.set_ylabel('y')
plt.colorbar(scatter)

fig.show()

input("Press Enter to continue...")

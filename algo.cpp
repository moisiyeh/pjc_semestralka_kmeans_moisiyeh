#include <cassert>
#include <mutex>
#include <thread>
#include <vector>
#include "algo.h"
#include "Point.h"

void kmeans_step(size_t n_begin, size_t n_end, const Point points[], size_t cluster_ids[]
    , size_t K, const Point centroids[/*K*/], Point centros[/*K*/], size_t counts[/*K*/]) {


    // Assign each data points to the cluster it is closest to
    for (auto n = n_begin; n < n_end; ++n) {
        size_t cluster_id = 0;
        Point::dist_t min_dist = -1;
        for (size_t k = 0; k < K; ++k) {
            const auto dist = Point::distance2(points[n], centroids[k]);
            if (min_dist < 0 || dist < min_dist) {
                min_dist = dist;
                cluster_id = k;
            }
        }
        cluster_ids[n] = cluster_id;

        centros[cluster_id] += points[n];
        counts[cluster_id] += 1;     // we will divide by this value when finalizing all threads' results
    }
}

size_t kmeans(size_t N, const Point points[/*N*/], size_t cluster_ids[/*N*/], size_t K, Point centroids[/*K*/], size_t num_threads) {

    if (num_threads == 0)
        num_threads = 1;

    // Let's choose initial cluster centers from input points
    const auto step = N / (K - 1);
    for (size_t k = 0; k < K; ++k)
        centroids[k] = points[k * step];

    const size_t chunk = N / num_threads;
    size_t iter = 1;
    for (; iter <= 1000; ++iter) {

        struct thread_result {
            std::shared_ptr<Point[]> centros;
            std::shared_ptr<size_t[]> counts;
            thread_result() = default;
            thread_result(size_t K): centros(new Point[K]), counts(new size_t[K]) {
                for (size_t k = 0; k < K; ++k)
                    counts.get()[k] = 0;
            }
        };
        std::vector<thread_result>  results;
        std::mutex                  results_access;

        std::vector<std::thread> threads;

        for (size_t t = 0; t < num_threads; ++t) {
            // The points in range [start..end) will be processed by the thread
            const auto start    = t * chunk;
            // For last thread the end index is the end of dataset
            const auto end      = (t == num_threads - 1) ? N : (t + 1) * chunk;
            threads.emplace_back([&, start, end]() {

                thread_result result(K);

                kmeans_step(start, end, points, cluster_ids, K, centroids, result.centros.get(), result.counts.get());

                std::lock_guard<std::mutex> guard(results_access);
                results.emplace_back(result);
            });
        }

        // Wait for all threads to finish
        for (auto& thread : threads)
            thread.join();

        std::vector<Point> centroids_next(K);
        for (size_t k = 0; k < K; ++k) {
            size_t count = 0;
            for (const auto& result : results) {
                centroids_next[k]   += result.centros.get()[k];
                count               += result.counts.get()[k];
            }
            if (count) {
                centroids_next[k] /= count;
            }
            else
                centroids_next[k] = centroids[k];   // keep the current centroid
        }

        // Check whether centroids have moved in this iteration
        Point::dist_t moved = 0;
        for (size_t k = 0; k < K; ++k) {
            moved += Point::distance2(centroids[k], centroids_next[k]);
        }
        if (moved < 0.01 * 0.01) {  // if cumulative distance is too small ...
            break;  // ... it means the algo has converged at last, so we leave the loop
        }

        std::copy(centroids_next.begin(), centroids_next.end(), centroids);
    }
    return iter;
}

#include <algorithm>
#include <vector>

#include "Dataset.h"

void Dataset::read(std::istream& is) {
    std::vector<Point> points;
    Point pt;
    while (is >> pt) {
        points.push_back(pt);
    }
    if (points.empty()) {
        this->_points.reset();
        this->_size = 0;
    }
    else {
        this->_size = points.size();
        this->_points.reset(new Point[this->_size]);
        std::copy(points.begin(), points.end(), this->_points.get());
    }
}

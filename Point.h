#pragma once
#include <cmath>

#include <iostream>

struct Point {
    typedef double coord_t;
    typedef double dist_t;

    coord_t x = 0;
    coord_t y = 0;

    const Point& operator += (const Point& other) {
        x += other.x;
        y += other.y;
        return *this;
    }
    const Point& operator /= (size_t n) {
        x /= n;
        y /= n;
        return *this;
    }

    void reset() {
        x = y = 0;
    }

    static dist_t distance(const Point& p1, const Point& p2) {
        return sqrt(distance2(p1, p2));
    }
    static dist_t distance2(const Point& p1, const Point& p2) {
        const auto d2 = (p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y);
        return d2;
    }
};
inline std::istream& operator >> (std::istream& is, Point& obj) {
    return is >> obj.x >> obj.y;
}
inline std::ostream& operator << (std::ostream& os, const Point& obj) {
    return os << obj.x << ' ' << obj.y;
}
